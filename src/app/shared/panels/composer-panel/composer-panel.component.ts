import { OnInit, Component } from '@angular/core';
import { TaskService } from 'src/app/task.service';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';

@Component({
  selector: 'app-composer-panel',
  templateUrl: './composer-panel.component.html',
  styleUrls: ['./composer-panel.component.css']
})

export class ComposerPanelComponent implements OnInit {

  events: any[];
  options: any;

  constructor(private taskService: TaskService) {

    this.options = {
      defaultDate: '2017-02-01',
      header: {
        plugins: [dayGridPlugin, timeGridPlugin, interactionPlugin],
          left: 'prev,next',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
      }
    };

    this.events = [
      {
          'title': 'All Day Event',
          'start': '2016-01-01'
      },
      {
          'title': 'Long Event',
          'start': '2016-01-07',
          'end': '2016-01-10'
      },
      {
          'title': 'Repeating Event',
          'start': '2016-01-09T16:00:00'
      },
      {
          'title': 'Repeating Event',
          'start': '2016-01-16T16:00:00'
      },
      {
          'title': 'Conference',
          'start': '2016-01-11',
          'end': '2016-01-13'
      }
    ];
  }

  ngOnInit() {


  }

}
